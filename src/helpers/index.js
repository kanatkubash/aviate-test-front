import { chunk } from 'lodash';

export const chunkNumber = str =>
  chunk(str.split().reverse(), 3)
    .map(s => s.join())
    .join(' ');
